---
title: Qualifications for competing in the Kingdom A&S Competition
subtitle: Updated Nov 1, 2011 by Kingdom MoAS
---
<p>The below outlines the basic requirements of competing in the Kingdom A&amp;S Competition. The complete rules and judging criteria can be found at the A&amp;S portion of the Drachenwald website. Questions should be addressed to MOAS Email  . Bear in mind that the below requirements are for persons wishing to compete for the Kingdom Championship and become Royal Artisan; all are welcome to enter items in any competition event, whether going for the Championship or not.</p>

<strong>Requirements</strong>
<ul>
<li> Item must have been completed while residing in Kingdom </li>
<li> Item cannot have been entered in a previous Kingdom-level event. </li>
<li> Include documentation (see below) </li>
<li> Include a competition form </li>
<li> Be received by the MoAS in charge of the event NLT event deadline (normally the morning the event starts). </li>
</ul>
<p><strong>Documentation must be</strong></p>
<ul>
<li> Written in English </li>
<li> Include at a minimum:
<ol>
<li> SCA Name </li>
<li> Mundane Name </li>
<li> Membership Number </li>
<li> SCA Group Name </li>
<li> Name of Item </li>
<li> Category </li>
<li> Description of Item </li>
<li> Description of Materials Used </li>
<li> Description of Manufacturing Process </li>
<li> Country of Origin </li>
<li> Period of Origin </li>
<li> Sources </li>
</ol>
</li>
<li> Written in 10 or 12 pitch, easy-to-read font </li>
<li> Include 4 copies of documentation </li>
</ul>
<p><strong>Judging</strong></p>
<ul>
<li> Judges will sign judging sheets </li>
<li> Will use judging criteria posted on Drachenwald A&amp;S website </li>
<li> Judging sheets are private and will be turned in to MoAS in charge of event </li>
<li> Judging sheets will be given to each entrant after scores are recorded </li>
<li> Copies of judging sheets will be retained by Kingdom MoAS </li>
<li> Can be made available for individual feedback; time permitting </li>
</ul>
<p><strong>Scoring</strong></p>
<ul>
<li> Maximum points for any item is 100 </li>
<li> If submitting multiple items in one category, only the top scoring item from each general category will be considered for Championship </li>
<li> Must enter an item in at least three of the six general categories (culinary arts, fine arts, sciences, textile arts, performance arts, and research paper) </li>
<li> Must enter an item in at least two of the five Competition Events </li>
<li> Entrants may send entries to a Competition Event, but must inform MoAS in advance and appoint someone to proxy their entry. </li>
<li> Scores will be posted on the Kingdom A&amp;S website </li>
</ul>
<p><strong>Competition Events</strong></p>
<ul>
<li> Will be held at each kingdom-level event (Twelfth Night Coronation, Spring Crown Tourney, Mid-summer Coronation, and Fall Crown Tourney) </li>
<li> The person who receives the highest score, will be declared the winner and awarded a token. </li>
<li> The winner of each competition event will receive an additional 5 points toward the Royal Artisan Competition</li>
<li> The highest score in each general category will be recognized and awarded a token. </li>
<li> The Kingdom MoAS, regional deputy, or their designated representative will be in charge of running each competition event. </li>
</ul>
<p><strong>Championship / Royal Artisan</strong></p>
<ul>
<li> Overall Champion is person who receives the highest combined score over course of year </li>
<li> Champion will be designated Royal Artisan, be inducted into the Order of Leonardo da Vinci, and assume the responsibility of assisting the Queen and Kingdom MoAS with promoting the Arts and Sciences </li>
<li> Champion will receive a medal with the badge of the Order of Leonardo da Vinci and a scroll celebrating their achievement </li>
<li> The Championship will be awarded each year at Kingdom University </li>
<li> A winner from each general category will be recognized at Kingdom University </li>
<li> Each general category winner will receive a token acknowledging their achievement </li>
</ul>
