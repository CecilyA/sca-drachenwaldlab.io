---
title: "Activities"
excerpt: "What do we do in Drachenwald?"
---
## Archery

With archery we seek to foster the skills developed for entertainment, hunting, and combat as practiced across the medieval period. Archery equipment and techniques varied widely across the area from western Europe, through the middle east to China and Japan, providing a great deal of options for interested archers to explore and reseach.  We use traditional bows and wooden arrows with feather fletchings for target and field shoots.

You might also be interested in:

- Bowering - The science of making Bows.
- Fletching - The making of arrows.
- Leatherworking - for making quivers, bags, boots, belts etc.

[Find out More]({{ site.baseurl }}{% link activities/archery.md %}){: .btn .btn--inverse}

## Armoured Combat 

Each of our groups (shires and colleges) have members who are intested in all aspects of medieval life, including medieval fighting styles. Not every group will have a weekly practice, but all will cater for activities at occasional day practices, special weekend practices or at events and revels.

In the SCA we have Armoured Combat, also known as heavy fighting, where participants armour up and fight with rattan swords. 

You might also be interested in:
  
  - Armouring
  - Leatherworking for armour, strapping and so on.
  - Metalworking and Smithing 

[Find out More]({{ site.baseurl }}{% link activities/armoured-combat.md %}){: .btn .btn--inverse}
 
## Arts and Sciences - Art, crafts, engineering, research and manufacture

The SCA has been described as the largest medieval craft and research self-help group in the world, and that does not fall short of the mark. The ethos of sharing one’s knowledge is very strong. People join with whatever skills they have and quickly find that there is great opportunity to learn and improve, to ask questions, to have impromptu workshops and attend a huge range of classes. Want just a taste of what we do?  

  - Bookbinding - Creating books by hand.
  - Calligraphy - Writing styles from a wide period.
  - Candle Making - Medieval-style candles from wax and tallow.
  - Carving - Carving ivory, wood, antler, and bone.
  - Casting - Casting jewellery, axe heads, candlesticks etc.
  - Ceramics & Pottery - Period pottery vessels and art pieces.
  - Dyeing - Using period materials and techniques to dye fabric.
  - Embroidery & Needlework - Medieval-style embroidered and needlework pieces.
  - Gaming - There are hundreds of board and card games to try.
  - Illumination - Medieval-style page decoration, often using gold leaf.
  - Leatherworking - Making Medieval leather items, bags, boots, belts etc.
  - Nalbinding and Knitting - Medieval styles and techniques.
  - Metalworking and Smithing - Making cutlery and daggers, forged iron or steel items etc. 
  - Spinning and Weaving - Making thread and fabric from raw materials.
  - Wood Working - Medieval carpentry: benches, techniques, etc. 

[Find out More]({{ site.baseurl }}{% link activities/artsci.md %}){: .btn .btn--inverse}

##  Medieval Fencing 

Fencing is the perfect way to study, replicate and compete with styles of light, heavy and cut-and-thrust weapon sword-fighting found in Europe during the Renaissance period.  This is not fencing in the Olympic style, this is based on ever expanding avenues of research with actual historical manuals.  It is an exciting, fast sport with plenty of style - fighter fight in period clothes - where fighters use blunted steel swords and a variety of off-hand defensive items.  Want to find out just how historically correct your favourite fight scene from a movie is? We have people who can tell you, and recommend texts to recreate your favourite fight more authentically!  

[Find out More]({{ site.baseurl }}{% link activities/fencing.md %}){: .btn .btn--inverse}

## How we get together - Events and Day revels 

Throughout the year there are several weekends or day event (revels) held across all our different groups (shires and colleges) These are where we dress in medieval appropriate attire, gather to engage in medieval appropriate activities and eat, drink and be merry with period feasting and entertainments. Different events might focus on different activities; for example &quot;Thrust, Piva, Riposte!&quot;	is	a	rapier	and	dancing	event	in	Scotland,	held by the Shire of Harpelstance once a year. Flaming Arrow is an archery event held in Ireland by the Shire of Glen Rathlin, also once a year. Many others are more general, offering a little of everything we do in all our local groups, so you should be able to find something in your area.

Revels are usually day long events, usually aimed at giving newcomers and interested parties a taste of the sort of thing we do. At these events we engage in a mix of activities

[Upcoming events]({{ site.baseurl }}{% link events/calendar.html %}){: .btn .btn--inverse}